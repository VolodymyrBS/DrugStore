﻿using System;
using System.Collections;
using System.Collections.Generic;

public class AbstractDisease : AbstractSaverNode
{
	private List<string> _symptoms;
	private List<DrugDose> _drugPrescription;
	private Treatment _treatment;
	private BaseTreatmentsList _treatmentsList;

	public List<string> Symptoms
	{
		get {
			return _symptoms;
		}
		set {
			_symptoms = value;
		}
	}

	public List<DrugDose> DrugPrescription
	{
		get {
			return this._drugPrescription;
		}
		set {
			_drugPrescription = value;
		}
	}

	public Treatment Treatments
	{
		get {
			return this._treatment;
		}
		set {
			_treatment = value;
		}
	}

	public AbstractDisease()
	{
		_treatmentsList = new BaseTreatmentsList();
	}

	public void Parse(string data)
	{
		throw new NotImplementedException();
	}

	public override void Parce(Dictionary<string, object> data)
	{
		foreach (var field in data.Keys) {
			switch (field) {
				case "symptoms":
					Symptoms = ((List<string>)data[field]);
					break;
				case "treatments":
					int id;
//					if (Int32.TryParse((string) data[field], out id))
//						_treatment = BaseTreatmentsList.Find(id);
					break;
				case "prescription":
					
					break;
			}
		}
	}

	public override Dictionary<string, object> Serialize()
	{
//		return new Dictionary<string, string>() {
//			{ "symptoms", Symptoms.ToArray() },
//
//		};
		return new Dictionary<string, object>();
	}
}
