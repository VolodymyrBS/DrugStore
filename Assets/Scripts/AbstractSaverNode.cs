﻿using System;
using System.Collections.Generic;

public abstract class AbstractSaverNode : ISaverNode
{
	protected int _id;
	protected ISaver _saver;

	public int id
	{
		get {
			return _id;
		}
		set {
			_id = value;
		}
	}

	public ISaver Saver
	{
		set {
			_saver = value;
		}
	}

	public virtual void Parce(Dictionary<string, object> data)
	{
		throw new NotImplementedException();
	}

	public virtual Dictionary<string, object> Serialize()
	{
		throw new NotImplementedException();
	}
}
