﻿using System;
using System.Collections.Generic;

public enum Period
{
	Hour,
	Day
}

public class DrugDose : AbstractSaverNode
{
	private BaseDrug _drug;
	private int _count;
	private Period _period;

	public BaseDrug Drug
	{
		get {
			return this._drug;
		}
		set {
			_drug = value;
		}
	}

	public int Count
	{
		get {
			return this._count;
		}
		set {
			_count = value;
		}
	}

	public Period Period
	{
		get {
			return this._period;
		}
		set {
			_period = value;
		}
	}

	public override void Parce(Dictionary<string, object> data)
	{
		throw new NotImplementedException();
	}

	public override Dictionary<string, object> Serialize()
	{
		throw new NotImplementedException();
	}
}

public enum DrugType
{
	Tablets,
	Ointment,
	Aids,
}

public class BaseDrug : AbstractSaverNode
{
	private string _name;
	private int _count;
	private List<BaseDrug> _analogs;
	private DrugDose _dose;
	private float _price;
	protected DrugType _type;

	public string Name
	{
		get {
			return this._name;
		}
		set {
			_name = value;
		}
	}

	public int Count
	{
		get {
			return _count;
		}
		set {
			_count = value;
		}
	}

	public List<BaseDrug> Analogs
	{
		get {
			return _analogs;
		}
		set {
			_analogs = value;
		}
	}

	public DrugDose Dose
	{
		get {
			return _dose;
		}
		set {
			_dose = value;
		}
	}

	public float Price
	{
		get {
			return _price;
		}
		set {
			_price = value;
		}
	}

	public DrugType Type
	{
		get {
			return this._type;
		}
	}

	public override void Parce(Dictionary<string, object> data)
	{
		throw new NotImplementedException();
	}

	public override Dictionary<string, object> Serialize()
	{
		throw new NotImplementedException();
	}


}
