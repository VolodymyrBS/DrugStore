﻿using System;

public interface IDragShopController
{
	void Add(BaseDrug drug);
	void Add(AbstractDisease disease);
	void Remove(BaseDrug drug);
	void Remove(AbstractDisease disease);
}
