﻿using System;
using System.Collections.Generic;

public class DrugNodeFactory : ISaverNodeFactory
{
	public ISaverNode Parce(Dictionary<string, object> data)
	{
		return new BaseDrug();
	}
}

public class DrugList : BaseSavableList<BaseDrug>
{
	public DrugList(ISaver saver) : base(saver)
	{
		_factory = new DrugNodeFactory();

	}
	
}
