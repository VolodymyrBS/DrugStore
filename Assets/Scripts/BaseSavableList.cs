﻿using System;
using System.Collections;
using System.Collections.Generic;

public class BaseSaverNode : ISaverNode
{
	private int _id;
	private ISaver _saver;

	public Dictionary<string, object> Serialize()
	{
		return new Dictionary<string, object>();
	}

	public int id
	{
		get {
			return _id;
		}
		set {
			_id = value;
		}
	}

	public ISaver Saver
	{
		set {
			_saver = value;
		}
	}

	public void Parce(Dictionary<string, object> data) {}
}

public class SimpleSaverNodeFactory : ISaverNodeFactory
{
	public ISaverNode Parce(Dictionary<string, object> data)
	{
		return new BaseSaverNode();
	}
}

public class BaseSavableList<T> where T : ISaverNode
{

	protected ISaver _saver;
	protected List<ISaverNode> _nodeList = new List<ISaverNode>();
	protected ISaverNodeFactory _factory = new SimpleSaverNodeFactory();
	private static T _instance;

	public BaseSavableList(ISaver saver = null, ISaverNodeFactory factory = null)
	{
		_saver = saver;
		_factory = factory;
	}

	public virtual T Find(int id)
	{
		return (T)_saver.GetNode(id);
	}

	public virtual T[] Find(SearchDelagate searchDelegate)
	{
		return Array.ConvertAll<ISaverNode, T>(_saver.FindNode(searchDelegate), new Converter<ISaverNode, T>(Convert));
	}

	private static T Convert(ISaverNode input)
	{
		return (T)input;
	}
}
