﻿using System;
using System.Collections.Generic;
using System.Collections;

public interface ISaverNode
{
	int id {
		get;
		set;
	}

	ISaver Saver {
		set;
	}

	void Parce(Dictionary<string, object> data);
	Dictionary<string, object> Serialize();
}

public interface ISaverNodeFactory
{
	ISaverNode Parce(Dictionary<string, object> data);
}

public delegate void SearchDelagate(ISaverNode obj);

public interface ISaver : IEnumerable<ISaverNode>
{
	/// <summary>
	/// The name of table with data
	/// </summary>
	string Name {
		get;
		set;
	}

	ISaverNodeFactory Factor {
		get;
		set;
	}

	void SaveNode(string nodeName, ISaverNode node);

	void SaveNode(ISaverNode node);

	ISaverNode GetNode(int id);

	ISaverNode[] FindNode(SearchDelagate pattern);

	ISaverNode[] LoadAllNodes();
}

